<?php

namespace Drupal\ifeed_importer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an IfeedImporter annotation object.
 *
 * @Annotation
 */
class IfeedImporter extends Plugin {

    /**
     * The human-readable name.
     *
     * @var \Drupal\Core\Annotation\Translation
     *
     * @ingroup plugin_translatable
     */
    public $label;

    /**
     * A description of the plugin.
     *
     * @var \Drupal\Core\Annotation\Translation
     *
     * @ingroup plugin_translatable
     */
    public $description;

}