<?php

namespace Drupal\ifeed_importer;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

class IfeedImporterManager extends DefaultPluginManager {

    /**
     * Default values for each plugin.
     *
     * @var array
     */
    protected $defaults = [
        'label' => '',
        'description' => '',
        'weight' => 0,
    ];

    /**
     * Constructs a new IfeedImporterManager object.
     *
     * @param \Traversable $namespaces
     *   An object that implements \Traversable which contains the root paths
     *   keyed by the corresponding namespace to look for plugin implementations.
     * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
     *   Cache backend instance to use.
     * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
     *   The module handler.
     */
    public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
        parent::__construct(
            'Plugin/IfeedImporter',
            $namespaces,
            $module_handler,
            'Drupal\ifeed_importer\Plugin\IfeedImporter\IfeedImporterInterface',
            'Drupal\ifeed_importer\Annotation\IfeedImporter'
        );
        $this->setCacheBackend($cache_backend, 'ifeedimporter_plugins');
    }


}