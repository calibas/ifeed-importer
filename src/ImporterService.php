<?php

namespace Drupal\ifeed_importer;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\StreamHandler;

/**
 * iFeed's Importer Service.
 */
class ImporterService
{

    use StringTranslationTrait;

    /**
     * Fetches a page
     */
    public function getPage($url)
    {
        //$handler = new StreamHandler;
        $client = \Drupal::httpClient();
        $options = [
            //'handler' => $handler,
            'headers' => [
                'User-Agent' => 'iFeedBot (+https://ifeed.social/)'
            ]
            //'Accept' => 'application/xml,application/rss+xml'
            //'Accept-Encoding' => 'gzip, deflate, br',
            //'Accept-Language' => 'en-US,en;q=0.5'
            //'Accept-Encoding' => 'gzip;q=0,deflate,sdch'
        ];
        $request = $client->request('GET', $url, $options);
        //foreach ($request->getHeaders() as $name => $values) {
        //    \Drupal::logger('gfp_crawler')->notice($name . ': ' . implode(', ', $values) . "\r\n");
        //}
        $response = (string) $request->getBody();
         return $response;
    }

    /**
     * Checks for duplicate URLs
     */
    public function checkDuplicateUrl($url) {
        $url = substr($url, 5);
        return db_select('node_revision__field_original_article', 'n')
            ->fields('n', array('entity_id'))
            //->condition('n.field_url_uri', $termname)
            ->condition('n.field_original_article_uri', '%' . db_like($url), 'LIKE')
            ->condition('n.deleted', 0)
            ->execute()
            ->fetchField();
    }

    public function saveImage($imageUrl) {
        $client = \Drupal::httpClient();
        $request = $client->get($imageUrl, ['User-Agent' => 'iFeedBot (+https://ifeed.social/)', 'Accept-Encoding' => 'gzip;q=0,deflate,sdch']);
        try {
            $data = $request->getBody();
            //$height = GDToolKit::getHeight($data);
            $filename = hash("crc32", $imageUrl);
            $imgDirectory = 'public://external-images/';
            if (file_prepare_directory($imgDirectory, FILE_CREATE_DIRECTORY)) {
                $file = file_save_data($data, $imgDirectory . $filename . '.jpg', FILE_EXISTS_REPLACE);
                $image = \Drupal::service('image.factory')->get($file->getFileUri());
                $image->convert('jpeg');
                $image->scale(550, 550);
                $image->save();
                return $file->id();
            } else {
                return false;
            }
        }
        catch (RequestException $e) {
            //watchdog_exception('gfp_crawler', $e);
            \Drupal::logger('gfp_crawler')->error($e);
            return false;
        }
    }

    public function cleanHTML($string, $encoding = 'utf-8') {
        $string = @mb_convert_encoding($string, $encoding, mb_detect_encoding($string));
        $string = @mb_convert_encoding($string, 'html-entities', $encoding);
        return $string;
    }

    public function getOGMeta($url) {
        $page = $this->getPage(trim($url));
        $cleanPage = $this->cleanHTML($page);
        $dom = new \DOMDocument();
        @$dom->loadHTML($cleanPage);
        $xp = new \DOMXPath($dom);
        $meta = new \stdClass();
        if($xp->query('//meta[@property="og:title"]/@content')->item(0)) {
            $meta->title = $xp->query('//meta[@property="og:title"]/@content')->item(0)->nodeValue;
        } else {
            $meta->title = $xp->query('//title')->item(0)->nodeValue;
        }
        if($xp->query('//meta[@property="og:description"]/@content')->item(0)) {
            $meta->preview = $xp->query('//meta[@property="og:description"]/@content')->item(0)->nodeValue;
        } else {
            if($xp->query('//p')->item(0)) {
                $previewGuess = $xp->query('//p')->item(0)->nodeValue;
                $previewGuess = strip_tags($previewGuess, '<p>');
                $periodPos = strpos($previewGuess, '. ', 200);
                $meta->preview = substr($previewGuess, 0, $periodPos + 1) . '..';
            }
        }
        if($xp->query('//meta[@property="og:image"]/@content')->item(0)) {
            $meta->imageURL = $xp->query('//meta[@property="og:image"]/@content')->item(0)->nodeValue;
        } else {
            $meta->imageURL = '';
        }
        \Drupal::logger('ifeed_importer')->notice('meta <pre>' . print_r($meta, true) . '</pre>');
        return $meta;
    }
}