<?php

namespace Drupal\ifeed_importer\Plugin\QueueWorker;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\Cache;

/**
 *  Queue worker for content imports
 *
 * @QueueWorker(
 *   id = "ifeed_import_queue",
 *   title = @Translation("iFeed Import Queue"),
 *   cron = {"time" = 10}
 * )
 */
class ImportQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface
{

    /**
     * @var \Drupal\Core\Database\Connection
     */
    protected $database;

    /**
     * Constructs a TeamCleaner worker.
     *
     * @param array $configuration
     * @param string $plugin_id
     * @param mixed $plugin_definition
     * @param \Drupal\Core\Database\Connection $database
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->database = $database;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('database')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function processItem($data)
    {
        $nid = isset($data->nid) && $data->nid ? $data->nid : NULL;
        if (!$nid) {
            throw new \Exception('Missing node ID');
            return;
        }

        $node = \Drupal::entityManager()->getStorage('node')->load($nid);
        //        //$url = $node->get('field_feed_source_url')->uri;
        $node->setChangedTime(time());
        $node->save();
        $importer_name = $node->get('field_importer')->value;

        //Default to RSS Importer
        if (empty($importer_name)) {
            $importer_name = 'rss_importer';
        }
        $importer_manager = \Drupal::service('plugin.manager.ifeedimporter');
        // Create a class instance through the manager.
        $importer_instance = $importer_manager->createInstance($importer_name);

        $importCount = $importer_instance->import($node);

        //
        if ($importCount) {
            Cache::invalidateTags(array('config:rest.resource.news_list_resource', 'ifeed_sourceentity_' . $nid));
        }

        //\Drupal::logger('ifeed_importer')->notice('<pre>' . print_r( $url, true) . '</pre>');

    }
}