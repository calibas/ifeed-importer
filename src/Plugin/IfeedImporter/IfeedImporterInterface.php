<?php

namespace Drupal\ifeed_importer\Plugin\IfeedImporter;

/**
 * Interface IfeedImporterInterface.
 */
interface IfeedImporterInterface {

    /**
     * Get the plugin's label.
     *
     * @return string
     *   The importer label
     */
    public function label();

    /**
     * Get the plugin's description.
     *
     * @return string
     *   The importer description
     */
    public function description();

    /**
     * Imports data from a URL
     *
     * @param Node $node
     *   The Feed Source to import from.
     *
     * @return int
     *   The number of items imported.
     */
    public function import($node);

}