<?php

namespace Drupal\ifeed_importer\Plugin\IfeedImporter;

use Drupal\node\Entity\Node;
use Drupal\Core\Plugin\PluginBase;

/**
 * RSS Importer.
 *
 * @IfeedImporter(
 *   id = "rss_importer",
 *   label = "RSS Importer",
 *   description = "",
 *   weight = -10
 * )
 */
class RSSImporter extends PluginBase implements IfeedImporterInterface
{

    /**
     * {@inheritdoc}
     */
    public function label()
    {
        return $this->pluginDefinition['label'];
    }

    /**
     * {@inheritdoc}
     */
    public function description()
    {
        return $this->pluginDefinition['description'];
    }

    /**
     * {@inheritdoc}
     */
    public function import($source)
    {
        $url = $source->get('field_feed_source_url')->uri;
        $parser = $source->get('field_page_parser')->value;
        $service = \Drupal::service('ifeedimporter.importer');
        $importCount = 0;
        $page = $service->getPage($url);
        $dom = new \DOMDocument();
        @$dom->loadXML($page);
        $xp = new \DOMXPath($dom);
        $xp->registerNamespace('media', 'http://search.yahoo.com/mrss/');
        // //item/link
        $feeds = $xp->query('//item');
        foreach ($feeds as $feed) {
            $itemUrl = $xp->query('.//link', $feed)->item(0)->nodeValue;
            if ($service->checkDuplicateUrl($itemUrl)) {
                continue;
            }
            if ($parser == 'ogmeta') {
                $pageMeta = $service->getOGMeta($itemUrl);
                $title = $pageMeta->title;
                $preview = $pageMeta->preview;
                $imageURL = $pageMeta->imageURL;
            } else {
                // Create node object with attached file.
                $title = $xp->query('.//title', $feed)->item(0)->nodeValue;
                $description = $xp->query('.//description', $feed)->item(0)->nodeValue;
                $preview = strip_tags($description, '<p>');
                // Look for an image
                $imageURL = '';
                $enclosures = $xp->query('.//enclosure', $feed);
                foreach ($enclosures as $enclosure) {
                    if (substr($enclosure->getAttribute('type'), 0,5 ) == 'image') {
                        $imageURL =$enclosure->getAttribute('url');
                    }
                }
                if(!$imageURL) {
                    $imageXpath = $xp->query('.//media:content/@url', $feed);
                    if ($imageXpath->item(0)) {
                        $imageURL = $imageXpath->item(0)->nodeValue;
                    } else {
                        $doc = new \DOMDocument();
                        @$doc->loadHTML($description);

                        $tags = $doc->getElementsByTagName('img');
                        $imageURL = ($tags) ? $tags[0]->getAttribute('src') : '';
                    }
                }
            }

            $node = Node::create([
                'type' => 'external_link',
                'title' => $title,
                //'field_image' => [
                //  'target_id' => $file->id(),
                //  'alt' => 'Article image',
                //  'title' => 'Article image'
                //],
                //'field_tags'	=> $tags,
                //'field_author'=> $term,
                'body' => array(
                    'value' => $preview,
                    'format' => 'basic_html',
                ),
                'field_original_article' => $itemUrl,
                'field_source' => $source->id()
            ]);
            //\Drupal::logger('ifeed_importer')->notice((string)$imageURL);
            if ($imageURL) {
                $fid = $service->saveImage($imageURL);
                if ($fid) {
                    $node->set('field_image', [
                        'target_id' => $fid,
                        'alt' => 'Article image',
                        'title' => 'Article image'
                    ]);
                }
            }

            // Look for a date
            $date = $xp->query('.//pubDate', $feed)->item(0)->nodeValue;
            if ($date) {
                $timestamp = strtotime($date);
                if ($timestamp < time()) {
                    $node->set('created', $timestamp);
                }
            }

            $node->save();
            $importCount++;

        }
        \Drupal::logger('ifeed_importer')->notice('<pre>' . print_r($page, true) . '</pre>');
        return $importCount;
    }

}